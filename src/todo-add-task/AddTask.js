import React, { Component } from 'react';
import './AddTask.css';

class AddTask extends Component {
  componentDidUpdate(prevProps) {
    if (prevProps.currentTask.text !== '') {
      this.props.inputElement.current.focus();
    }
  }
  render() {
    return (
      <form className="form" onSubmit={this.props.addTask}>
        <input placeholder="Task"
          ref={this.props.inputElement}
          value={this.props.currentTask.text}
          onChange={this.props.handleInput}/>
        <button className="btn" type="submit">Add Task</button>
      </form>
    );
  }
}

export default AddTask;
