import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import AddTask from './todo-add-task/AddTask';
import TasksList from './todo-tasks-list/TasksList';

class App extends Component {
  constructor() {
    super()
    this.state = {
      tasks: [
        {text:'P versus NP problem', status: false, key:1971},
        {text:'Birch and Swinnerton-Dyer conjecture', status: false, key:1960},
        {text:'Yang–Mills theory', status: false, key:1954},
        {text:'Hodge conjecture', status: false, key:1941},
        {text:'Poincaré conjecture', status: true, key:1900},
        {text:'Riemann hypothesis', status: false, key:1859},
        {text:'Navier–Stokes existence and smoothness', status: false, key:1821},
      ],
      currentTask: {text:'', status: false, key:''},
    }
    this.inputElement = React.createRef();
  }
  handleInput = e => {
    this.setState({
      ...this.state,
      currentTask: {
        ...this.state.currentTask,
        text: e.target.value
      }
    });
  }
  addTask = e => {
    e.preventDefault();
    if (this.state.currentTask.text !== '') {
      this.setState({
        tasks: [{...this.state.currentTask, key: Date.now()}].concat(this.state.tasks),
        currentTask: { text: '', status: false, key: '' },
      });
    }
  }
  taskDone = e => {
    const key = Number(e.target.value);
    this.setState({
      ...this.state,
      tasks: this.state.tasks.map(task => {
        if (task.key === key) {
          task.status = !task.status;
        }
        return task;
      })
    });
  }
  removeTask = key => e => {
    this.setState({
      ...this.state,
      tasks: this.state.tasks.filter(task => task.key !== key)
    });
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>ToDo List</h1>
        </header>
        <AddTask
          addTask={this.addTask}
          inputElement={this.inputElement}
          handleInput={this.handleInput}
          currentTask={this.state.currentTask}
        />
        <TasksList
          tasks={this.state.tasks}
          taskDone={this.taskDone}
          removeTask={this.removeTask}
        />
      </div>
    );
  }
}

export default App;
