import React, { Component } from 'react';
import './TasksList.css';

class TasksList extends Component {
  render() {
    const tasksList = this.props.tasks.map((task) => {
      return <div className="task" key={task.key}>
        <input type="checkbox"
          id={task.key}
          checked={task.status}
          value={task.key}
          onChange={this.props.taskDone}
        />
        <label htmlFor={task.key}>{task.text}</label>
        <span className="remove"
          onClick={this.props.removeTask(task.key)}
        >⤫</span>
      </div>
    });

    return <div className="list">{tasksList}</div>
  }
}

export default TasksList;
